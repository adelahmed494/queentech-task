<?php

namespace Tests\Feature;

// use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FileTest extends TestCase
{    
    /**
     * testGetFileWithoutPageParam
     * this method used to check what happen if not pass page param in the request.
     * @return void
     */
    public function testGetFileWithoutPageParam(){
        $param = ['per_page' => 10];
        $response = $this->call('GET','api/file',$param)
                    ->assertStatus(200);
    }
    
    /**
     * testGetFileWithoutPerPageParam
     * this method used to check what happen if not pass per page param in the request.
     * @return void
     */
    public function testGetFileWithoutPerPageParam(){
        $param = ['page' => 1];
        $response = $this->call('GET','api/file',$param)
                    ->assertStatus(200);
    }
        
    /**
     * testGetSuccessFile
     * test what happen if pass all param.
     * @return void
     */
    public function testGetSuccessFile(){
        $param = ['per_page' => 10,'page' => 1];
        $response = $this->call('GET','api/file',$param)
                    ->assertStatus(200);
    }

}

?>