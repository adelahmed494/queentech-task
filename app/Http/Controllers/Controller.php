<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Used to validate request
     */
    public function ValidateRequest($request,$rules){

        $validator = Validator::make($request,$rules);

        if ($validator->fails()) {
            $messages = $validator->messages();

            throw new HttpResponseException(response()->json([
                'success'   => false,
                'message'   => 'Validation errors',
                'data'      => $validator->messages()
            ]));

        }
    }

    /**
     * used to return error messages
     */
    public function errorMessage($message,$errorCode=''){

        throw new HttpResponseException(response()->json([
            'success'   => false,
            'message'   => $message
        ],$errorCode));   
    }

}
