<?php

namespace App\Http\Controllers;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Database\Eloquent\Collection;

use Illuminate\Http\Request;

class LogFileController extends Controller
{
    public $content;
    
    /**
     * __construct
     * get laravel log file content.
     * @return void
     */
    public function __construct(){
        $this->content = file(storage_path().'/logs/laravel.log');
    }
    
    /**
     * getFileContent
     * get file content based on per page and page number.
     * @param  mixed $request
     * @return void
     */
    public function getFileContent(Request $request){

        $this->ValidateRequest($request->all(),['per_page'   => 'required|numeric','page'   => 'required|numeric']);

        $data = $this->paginate($request->get('per_page'),$request->get('page'));
        return $data;
    }
    
    /**
     * paginate
     * paginate file content.
     * @param  mixed $perPage
     * @param  mixed $page
     * @param  mixed $options
     * @return void
     */
    public function paginate($perPage = 10, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $this->content = $this->content instanceof Collection ? $this->content : Collection::make($this->content);
        return new LengthAwarePaginator($this->content->forPage($page, $perPage), $this->content->count(), $perPage, $page, $options);
    }


}
