# QueenTech Task - Log File Viewer 

Web app that can view a specific file under the server log directory
eg. /logs/laravel.log and display at maximum 10 lines at a time.

## Technologies
Project is created with:
* php version 8.0.2
* laravel version: 9.19
* Vue js version 3
* php unit version 9.5.10

## Getting started
I used Laravel.log file as example to read it's content,

I changed in this files

* js\App.vue
* routes\api.php
* Controllers\LogFileController.php
* tests\FileTest.php

## Setup
To run this project, install it locally using composer:

```
$ git clone https://gitlab.com/adelahmed494/queentech-task.git
$ composer update
$ npm install
$ php artisan serve
$ npm run dev
```
then open http://127.0.0.1:8000/